## Level 1 Aktivitäten

# Aufgabe 1 - Einfache Versorgung

## Beschreibung: Langsames heranführen der Schüler/Studierenden an das arbeiten mit dem LEGOS Demonstrator, Dauer: ~ 10 min

- A) Stelle eine Verbingung zwischen einer Quelle und einem Verbraucher her.
- B) In welcher Beziehung stehen Strom, Spannung und Widerstand zueinander?
- C) Fließt ein Strom, wenn der Verbraucher nicht angeschlossen ist? Wenn ja, wie groß ist dieser ?
- D) Berechne die Spannung am Vebraucher.
- E) Was gilt für die Leistung am Verbraucher? Wovon hängt die Leistung ab ?

## Lösungen zur Aufgabe 1 - Einfache Versorgung

- A) Bild einblenden mit Quelle + Leitung + Verbraucher             **// Bild einfügen**
- B) Ohmsches Gesetz : U = R * I 
- C) Es fließt kein Strom. I = U / R. wenn R -> 0 dann geht auch I -> 0
- D) Spannung am Verbraucher ist in diesem Aufbau gegeben durch: U_quelle = U_verbraucher = R * I 
- E) Formel für die Leistung: P = U * I = R * I^2 = U^2 / R

# Aufbgabe 2 - Parallelschalten von Verbrauchern

## Beschreibung: Betrachtung des Stromflusses und Herleitung des Stromteilers, Dauer ~ 15-20 min

- A) Schalte zwei Verbraucher parallel zu einander. Was ergibt sich für die Spannung an den jeweiligen Verbrauchern? Was für den Strom?
- B) Zeichne das Ersatzschaltbild der Schaltung
- C) Stelle alle Maschen-&Knotengleichungen auf, die du findest.
- D) Stelle den Strom am Verbraucher in Abhängigkeit von der Stromquelle und den einzelnen Verbrauchern auf.

## Lösungen zur Aufgabe 2 - Parallelschalten von Verbrauchern
- A) Bild einer möglichen Schaltung zeigen. Spannungen sind genauso groß, wie die Spannung der Quelle. 𝑈_𝑞𝑢𝑒𝑙𝑙𝑒 = 𝑈_1 = 𝑈_2 = 𝑈_3 =…= 𝑈_𝑛 Der Strom hingegen teilt sich an den Knotenpunkten in Teilströme auf. ∑𝐻𝑖𝑛𝑒𝑖𝑛𝑓𝑙𝑖𝑒ß𝑒𝑛𝑑𝑒𝑟 𝑆𝑡𝑟ö𝑚𝑒= ∑𝐻𝑖𝑛𝑎𝑢𝑠𝑓𝑙𝑖𝑒ß𝑒𝑛𝑑𝑒𝑟 𝑆𝑡𝑟ö𝑚𝑒.                                                                         **// Bild einfügen**
- B) Im Vergleich zu einer Reihenschaltung, bleibt bei einer Parallelschaltung die Spannung gleich der Quellspannung. Bei einer Reihenschaltung hingegen, bleibt der fließende Strom durch die Verbraucher gleich groß.
- C) ESB einer Schaltung zeigen (eine von vielen Möglichkeiten)        ** // Bild einfügen**
- D) Aus dem Beispiel ESB alle Knoten und Maschengleichungen bestimmen
- E) Herleitung des Stromteilers zeigen. // **BILD EINFÜGEN**




