Level 3 Aktivitäten

# Aufgabe 1 - Aufbauen einer Ringnetzschaltung

## Beschreibung: Aufbauen eines komplexeren Netzwerkes und Betrachtung verschiedener KS, Dauer: ~ 25-60 min

- A) Baue eine Ringnetzschaltung.
- B) Kurzschließen von Verbindungen 
    - 1) Welche Auswirkungen hat ein Kurzschluss (KS) auf das Kraftwerk?
    - 2) Wie kann man den KS sicher beheben? Wie kann man die Versorgungssicherheit der Verbraucher sicherstellen ?
    - 3) Mehrere KS bilden & beheben 
- C) Welche Arten von UPS gibt es? Wo werden diese eingesetzt?


## Lösungen zur Aufgabe 1 - Aufbauen einer Ringnetzschaltung

- A) -Mögliches Bild einer Schaltung einblenden- **BILD EINFÜGEN**
- B) 
    - 1) Kurzschluss: Es entsteht ein hoher Kurzschlussstrom 𝐼_𝐾𝑆, Kraftwerk wird überlastet (leuchtet rot)
    - 2) Am Modell zeigen wie der KS sicher behoben werden kann. Verosrgungssicherheit bspw. durch entsprechende Netztopologie oder UPS
    - 3) Am Modell zeigen wie verschiedene KS behoben und die Verbraucher weiterhin versorgt werden können
- C) Arten von UPS: Generatoren und Batterien. Einsatzgebiete bspw. Krankenhäuser und Rechenzentren


