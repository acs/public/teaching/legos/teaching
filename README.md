# ![LEGOS](docs/legos_logo.png) <br/> Lite Emulator of Grid Operations

## Description
The Lite Emulator of Grid Operations (LEGOS) is an educational multi-layer platform for demonstrating smart energy service use cases in a real down-scaled power grid. LEGOS platform aims to bring a gamification approach in the Power Systems domain to address the novel education requirements of energy transition. In fact, through a down-scaled laboratory that offers non-virtual engaging interactions and a wide variety of multidisciplinary learning experiences, several activities can be tailored for different skills level of the target audience.

## Learning tasks
Tasks are organized in levels from 1 to 3 that increase with difficulty.
- Level 1
	- Task 1: this is an example [[en]](level_1/task_1/README.md) [[de]](level_1/task_1/README_de.md)
- Level 2
	- Task 1: this is an example [[en]](level_2/task_1/README.md) [[de]](level_2/task_1/README_de.md)
- Level 3
	- Task 1: this is an example [[en]](level_3/task_1/README.md) [[de]](level_3/task_1/README_de.md)
	
## Copyright

2021, Institute for Automation of Complex Power Systems, EONERC

## License

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.

## Funding
<a rel="funding" href="https://www.eddie-erasmus.eu/"><img alt="EDDIE" style="border-width:0" src="docs/EDDIE.png" width="223" height="63"/></a>&nbsp;<a rel="programme" href="https://www.erasmusplus.eu/"><img alt="Erasmus+" style="border-width:0" src="docs/erasmus.png" width="229" height="63"/></a><br />This work was supported by <a rel="eddie" href="https://www.eddie-erasmus.eu/">
EDucation for DIgitalisation of Energy</a> (EDDIE), an European Union’s Erasmus+ project funded under grant agreement 612398.

## Contact

[![EONERC ACS Logo](docs/eonerc_logo.png)](http://www.acs.eonerc.rwth-aachen.de)

- [Dr. Carlo Guarnieri Calò Carducci](mailto:cguarnieri@eonerc.rwth-aachen.de)

[Institute for Automation of Complex Power Systems (ACS)](http://www.acs.eonerc.rwth-aachen.de)  
[E.ON Energy Research Center (EONERC)](http://www.eonerc.rwth-aachen.de)  
[RWTH University Aachen, Germany](http://www.rwth-aachen.de)  

