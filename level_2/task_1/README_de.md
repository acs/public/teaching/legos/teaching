## Level 2 Aktivitäten

# Aufgabe 1 - Verbraucher wird vom Netz getrennt

## Beschreibung: Einführung in die Thematik der Kurzschlüsse, Dauer: ~ 20 min

- A) Wie können Kurzschlüsse entstehen?
- B) Welche Möglichkeiten gibt es, im Falle eines KS für eine Versorgungssicherheit zu sorgen?
- C) Schalte eine Kreisschaltung mit einer Quelle und einem Verbraucher.
- D) Schalte eine der Leitungen kurz. Was resultiert daraus für den Verbraucher und die Quelle?


## Lösungen zur Aufgabe 1 - Verbraucher wird vom Netz getrennt

- A) Mehrere Antwortmöglichkeiten möglich. Bspw.: Fehlerhafte/ beschädigte Isolierung der Kabel, Schaltfehler, Unwetter,…
- B) USV, Unterbrechungsfreiestromversorgung. Einsatzgebiete in Krankenhäusern, Servern, etc.
- C) Kreisschaltung darstellen mit Bildern.  **BILDER EINFÜGEN**
- D) Bei einer Kreisschaltung, dient einer der beiden Leiterstränge als Notfallstrang. Wird der Versorgerstrang kurzgeschaltet, wird dementsprechend auch die Versorgung des Verbrauchers unterbrochen. Des weiteren fließt ein sehr hoher Kurzschlussstrom, welcher die Quelle stark belastet. (𝐼_𝐾𝑆>𝐼_𝑁). Den Schalter umlegen -> Versorgung kann wieder gewährleistet werden, und der KS am anderen Strang kann behoben werden. 


# Aufgabe 2 - Zuschalten einer erneuerbaren Energiequelle

## Beschreibung: Betrachtung und Analyse einer EE-Anlage, Dauer: ~ 20 min

- A) Stelle eine Verbindung aus einem Kraftwerk, zwei Verbrauchern und einer erneuerbaren Energie her.
- B) Lasse die EE-Anlage zunächst ausgeschaltet. Wie verläuft die Versorgung für die einzelnen Verbraucher ab? Fließt ein Strom an den Verbraucher, welcher sich hinter der EE-Anlage befindet? Wenn nein, wieso nicht?
- C) Schalte die EE-Anlage ein. Betreibe diese auf halber bzw. voller Kraft. Welche Beobachtungen hinsichtlich des Versorgung lässt sich treffen?
- D) Zeichne das ESB und berechne die Spannung, Strom und Leistung an den jeweiligen Verbrauchern


## Lösungen zur Aufgabe 2 - Zuschalten einer erneuerbaren Energiequelle

- A) Mögliche Schaltung mit Bildern Schritt für Schritt darstellen.         **BILD EINFÜGEN**
- B) Der Verbraucher welcher sich zwischen Kraftwerk und EE-Anlage befindet, wird von beiden teilversorgt. Der Verbraucher welcher hinter die EE-Anlage geschaltet ist, wird nur von der EE-Anlage versorgt, nicht von dem Kraftwerk, (da Ströme entgegengesetzt fließen würden)
- C) Desto höher die EE-Anlage reguliert wird, desto mehr Strom kann diese an die Verbraucher liefern.
- D) Mögliches ESB zeichnen und Rechnungen vorstellen.      **BILD EINFÜGEN**
